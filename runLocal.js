const { handler: getComponentById } = require('./src/getComponentById');
const { handler: saveComponent } = require('./src/saveComponent');
const { handler: getComponentSets } = require('./src/getComponentSets');
const { handler: getComponentSetPropertiesById } = require('./src/getComponentSetPropertiesById');
const { handler: saveAsset } = require('./src/saveAsset');
const { handler: getAssetsByGameName } = require('./src/getAssetsByGameName');

const component = require('./testData/component.json');
const newComponent = require('./testData/newComponent.json');
const base64Asset = require('./testData/base64Asset.json');
const lambdaEventHeaders = require('./testData/lambdaEventHeaders.json');
const [, , endpoint] = process.argv;

const runHandler = async (endpoint) => {
    let response;
    switch (endpoint) {
    case 'getComponentById':
        response = await runGetComponentById();
        break;
    case 'saveComponent':
        response = await runSaveComponent();
        break;
    case 'runSaveComponentNew':
        response = await runSaveComponentNew();
        break;
    case 'test':
        response = await test();
        break;
    case 'getComponentSets':
        response = await runGetComponentSets();
        break;
    case 'getComponentSetPropertiesById':
        response = await runGetComponentSetPropertiesById();
        break;
    case 'saveAsset':
        response = await runSaveAsset();
        break;
    case 'getAssetsByGameName':
        response = await runGetAssetsByGameName();
        break;
    }
    
    console.log({ response });
    return process.exit(0);
};

const event = {
    requestContext: { stage: 'local' },
};

const runGetComponentSetPropertiesById = () => {
    event.body = '{"componentSetId": 1}';
    return getComponentSetPropertiesById(event);
};

const runGetComponentSets = () => {
    return getComponentSets();
};

const runSaveComponent = () => {
    event.body = JSON.stringify(component);
    console.log(event);
    return saveComponent(event);
};

const runSaveComponentNew = () => {
    event.body = JSON.stringify(newComponent);
    console.log(event);
    return saveComponent(event);
};

const runGetComponentById = () => {
    event.body = '{"componentId": 11}';
    return getComponentById(event);
};

const runSaveAsset = () => {
    event.body = base64Asset.body;
    event.headers = lambdaEventHeaders;
    return saveAsset(event);
};

const runGetAssetsByGameName = () => {
    event.body = '{"gameName": "Dungeon Game"}';
    return getAssetsByGameName(event);
};

const test = () => {
    // eslint-disable-next-line quotes
    const body = "{\"component\":{\"ComponentSetName\":\"Paladin\",\"ComponentName\":\"Penance\",\"ComponentWidth\":408,\"ComponentHeight\":585,\"ComponentSetId\":1,\"Properties\":[{\"ZPosition\":0,\"SecondarySort\":24,\"FontSize\":null,\"FontColor\":null,\"Width\":null,\"Height\":null,\"PropertyName\":\"background_color\",\"PropertyValue\":\"assets/paladin_background_color.png\",\"isVisible\":1,\"XPosition\":0,\"YPosition\":40,\"PropertyType\":\"image\",\"Alignment\":\"left\",\"ComponentSetPropertyId\":24,\"ComponentPropertyId\":24},{\"ZPosition\":1,\"SecondarySort\":23,\"FontSize\":null,\"FontColor\":null,\"Width\":null,\"Height\":null,\"PropertyName\":\"background_border\",\"PropertyValue\":\"assets/player_single_face.png\",\"isVisible\":1,\"XPosition\":0,\"YPosition\":0,\"PropertyType\":\"image\",\"Alignment\":\"left\",\"ComponentSetPropertyId\":23,\"ComponentPropertyId\":23},{\"ZPosition\":5,\"SecondarySort\":25,\"FontSize\":null,\"FontColor\":null,\"Width\":null,\"Height\":null,\"PropertyName\":\"foreground\",\"PropertyValue\":\"assets/paladin_foreground.png\",\"isVisible\":1,\"XPosition\":144,\"YPosition\":0,\"PropertyType\":\"image\",\"Alignment\":\"left\",\"ComponentSetPropertyId\":25,\"ComponentPropertyId\":25},{\"ZPosition\":8,\"SecondarySort\":26,\"FontSize\":null,\"FontColor\":null,\"Width\":null,\"Height\":null,\"PropertyName\":\"topTextIsCooldown\",\"PropertyValue\":\"assets/topTextIsCooldown.png\",\"isVisible\":1,\"XPosition\":28,\"YPosition\":38,\"PropertyType\":\"image\",\"Alignment\":\"left\",\"ComponentSetPropertyId\":26,\"ComponentPropertyId\":26},{\"ZPosition\":8,\"SecondarySort\":27,\"FontSize\":null,\"FontColor\":null,\"Width\":null,\"Height\":null,\"PropertyName\":\"bottomTextIsCooldown\",\"PropertyValue\":\"assets/bottomTextIsCooldown.png\",\"isVisible\":1,\"XPosition\":28,\"YPosition\":366,\"PropertyType\":\"image\",\"Alignment\":\"left\",\"ComponentSetPropertyId\":27,\"ComponentPropertyId\":27},{\"ZPosition\":10,\"SecondarySort\":1,\"FontSize\":18,\"FontColor\":null,\"Width\":null,\"Height\":null,\"PropertyName\":\"topName\",\"PropertyValue\":\"Penance\",\"isVisible\":1,\"XPosition\":204,\"YPosition\":252,\"PropertyType\":\"text\",\"Alignment\":\"center\",\"ComponentSetPropertyId\":1,\"ComponentPropertyId\":1},{\"ZPosition\":10,\"SecondarySort\":2,\"FontSize\":12,\"FontColor\":null,\"Width\":330,\"Height\":115,\"PropertyName\":\"topTextValue\",\"PropertyValue\":\"Paladin receives 4 direct damage (this damage may not be mitigated by any means).\",\"isVisible\":1,\"XPosition\":40,\"YPosition\":80,\"PropertyType\":\"text\",\"Alignment\":\"left\",\"ComponentSetPropertyId\":2,\"ComponentPropertyId\":2},{\"ZPosition\":10,\"SecondarySort\":3,\"FontSize\":26,\"FontColor\":\"#fff\",\"Width\":null,\"Height\":null,\"PropertyName\":\"topCooldownValue\",\"PropertyValue\":\"2\",\"isVisible\":1,\"XPosition\":40,\"YPosition\":222,\"PropertyType\":\"text\",\"Alignment\":\"left\",\"ComponentSetPropertyId\":3,\"ComponentPropertyId\":3},{\"ZPosition\":10,\"SecondarySort\":4,\"FontSize\":null,\"FontColor\":null,\"Width\":null,\"Height\":null,\"PropertyName\":\"topIsCooldownAbility\",\"PropertyValue\":\"assets/isCooldown.png\",\"isVisible\":1,\"XPosition\":220,\"YPosition\":222,\"PropertyType\":\"image\",\"Alignment\":\"left\",\"ComponentSetPropertyId\":4,\"ComponentPropertyId\":4},{\"ZPosition\":10,\"SecondarySort\":5,\"FontSize\":null,\"FontColor\":null,\"Width\":null,\"Height\":null,\"PropertyName\":\"topCastPosition\",\"PropertyValue\":\"\",\"isVisible\":0,\"XPosition\":190,\"YPosition\":228,\"PropertyType\":\"image\",\"Alignment\":\"center\",\"ComponentSetPropertyId\":5,\"ComponentPropertyId\":5},{\"ZPosition\":10,\"SecondarySort\":6,\"FontSize\":null,\"FontColor\":null,\"Width\":null,\"Height\":null,\"PropertyName\":\"topTargetPosition\",\"PropertyValue\":\"\",\"isVisible\":0,\"XPosition\":380,\"YPosition\":250,\"PropertyType\":\"image\",\"Alignment\":\"left\",\"ComponentSetPropertyId\":6,\"ComponentPropertyId\":6},{\"ZPosition\":10,\"SecondarySort\":7,\"FontSize\":null,\"FontColor\":null,\"Width\":null,\"Height\":null,\"PropertyName\":\"topIsReaction\",\"PropertyValue\":\"assets/isReaction.png\",\"isVisible\":1,\"XPosition\":15,\"YPosition\":202,\"PropertyType\":\"image\",\"Alignment\":\"left\",\"ComponentSetPropertyId\":7,\"ComponentPropertyId\":7},{\"ZPosition\":10,\"SecondarySort\":8,\"FontSize\":null,\"FontColor\":null,\"Width\":null,\"Height\":null,\"PropertyName\":\"topIsQuickReaction\",\"PropertyValue\":\"assets/isQuickAction.png\",\"isVisible\":1,\"XPosition\":61,\"YPosition\":242,\"PropertyType\":\"image\",\"Alignment\":\"left\",\"ComponentSetPropertyId\":8,\"ComponentPropertyId\":8},{\"ZPosition\":10,\"SecondarySort\":9,\"FontSize\":null,\"FontColor\":null,\"Width\":null,\"Height\":null,\"PropertyName\":\"topIsMoveAction\",\"PropertyValue\":\"assets/isMoveAction.png\",\"isVisible\":1,\"XPosition\":160,\"YPosition\":226,\"PropertyType\":\"image\",\"Alignment\":\"left\",\"ComponentSetPropertyId\":9,\"ComponentPropertyId\":9},{\"ZPosition\":10,\"SecondarySort\":10,\"FontSize\":null,\"FontColor\":null,\"Width\":null,\"Height\":null,\"PropertyName\":\"topSpecialAbility\",\"PropertyValue\":\"\",\"isVisible\":0,\"XPosition\":40,\"YPosition\":230,\"PropertyType\":\"image\",\"Alignment\":\"left\",\"ComponentSetPropertyId\":10,\"ComponentPropertyId\":10},{\"ZPosition\":10,\"SecondarySort\":11,\"FontSize\":null,\"FontColor\":null,\"Width\":null,\"Height\":null,\"PropertyName\":\"topSpecialValue\",\"PropertyValue\":\"\",\"isVisible\":0,\"XPosition\":50,\"YPosition\":230,\"PropertyType\":\"text\",\"Alignment\":\"left\",\"ComponentSetPropertyId\":11,\"ComponentPropertyId\":11},{\"ZPosition\":10,\"SecondarySort\":12,\"FontSize\":18,\"FontColor\":null,\"Width\":null,\"Height\":null,\"PropertyName\":\"bottomName\",\"PropertyValue\":\"Retrubution\",\"isVisible\":1,\"XPosition\":204,\"YPosition\":290,\"PropertyType\":\"text\",\"Alignment\":\"center\",\"ComponentSetPropertyId\":12,\"ComponentPropertyId\":12},{\"ZPosition\":10,\"SecondarySort\":13,\"FontSize\":12,\"FontColor\":null,\"Width\":330,\"Height\":115,\"PropertyName\":\"bottomTextValue\",\"PropertyValue\":\"While this ability is on cooldown, each enemy receives 2D magic damage at the start of Paladin's turn.\",\"isVisible\":1,\"XPosition\":35,\"YPosition\":400,\"PropertyType\":\"text\",\"Alignment\":\"left\",\"ComponentSetPropertyId\":13,\"ComponentPropertyId\":13},{\"ZPosition\":10,\"SecondarySort\":14,\"FontSize\":26,\"FontColor\":\"#fff\",\"Width\":null,\"Height\":null,\"PropertyName\":\"bottomCooldownValue\",\"PropertyValue\":\"-\",\"isVisible\":1,\"XPosition\":40,\"YPosition\":328,\"PropertyType\":\"text\",\"Alignment\":\"left\",\"ComponentSetPropertyId\":14,\"ComponentPropertyId\":14},{\"ZPosition\":10,\"SecondarySort\":15,\"FontSize\":null,\"FontColor\":null,\"Width\":null,\"Height\":null,\"PropertyName\":\"bottomIsCooldownAbility\",\"PropertyValue\":\"assets/isCooldown.png\",\"isVisible\":1,\"XPosition\":220,\"YPosition\":332,\"PropertyType\":\"image\",\"Alignment\":\"left\",\"ComponentSetPropertyId\":15,\"ComponentPropertyId\":15},{\"ZPosition\":10,\"SecondarySort\":16,\"FontSize\":null,\"FontColor\":null,\"Width\":null,\"Height\":null,\"PropertyName\":\"bottomCastPosition\",\"PropertyValue\":\"\",\"isVisible\":0,\"XPosition\":204,\"YPosition\":310,\"PropertyType\":\"image\",\"Alignment\":\"center\",\"ComponentSetPropertyId\":16,\"ComponentPropertyId\":16},{\"ZPosition\":10,\"SecondarySort\":17,\"FontSize\":null,\"FontColor\":null,\"Width\":null,\"Height\":null,\"PropertyName\":\"bottomTargetPosition\",\"PropertyValue\":\"\",\"isVisible\":0,\"XPosition\":380,\"YPosition\":310,\"PropertyType\":\"image\",\"Alignment\":\"left\",\"ComponentSetPropertyId\":17,\"ComponentPropertyId\":17},{\"ZPosition\":10,\"SecondarySort\":18,\"FontSize\":null,\"FontColor\":null,\"Width\":null,\"Height\":null,\"PropertyName\":\"bottomIsReaction\",\"PropertyValue\":\"assets/isReaction.png\",\"isVisible\":1,\"XPosition\":15,\"YPosition\":360,\"PropertyType\":\"image\",\"Alignment\":\"left\",\"ComponentSetPropertyId\":18,\"ComponentPropertyId\":18},{\"ZPosition\":10,\"SecondarySort\":19,\"FontSize\":null,\"FontColor\":null,\"Width\":null,\"Height\":null,\"PropertyName\":\"bottomIsQuickReaction\",\"PropertyValue\":\"assets/isQuickAction.png\",\"isVisible\":1,\"XPosition\":61,\"YPosition\":313,\"PropertyType\":\"image\",\"Alignment\":\"left\",\"ComponentSetPropertyId\":19,\"ComponentPropertyId\":19},{\"ZPosition\":10,\"SecondarySort\":20,\"FontSize\":null,\"FontColor\":null,\"Width\":null,\"Height\":null,\"PropertyName\":\"bottomIsMoveAction\",\"PropertyValue\":\"assets/isMoveAction.png\",\"isVisible\":1,\"XPosition\":160,\"YPosition\":336,\"PropertyType\":\"image\",\"Alignment\":\"left\",\"ComponentSetPropertyId\":20,\"ComponentPropertyId\":20},{\"ZPosition\":10,\"SecondarySort\":21,\"FontSize\":null,\"FontColor\":null,\"Width\":null,\"Height\":null,\"PropertyName\":\"bottomSpecialAbility\",\"PropertyValue\":\"\",\"isVisible\":0,\"XPosition\":40,\"YPosition\":290,\"PropertyType\":\"image\",\"Alignment\":\"left\",\"ComponentSetPropertyId\":21,\"ComponentPropertyId\":21},{\"ZPosition\":10,\"SecondarySort\":22,\"FontSize\":null,\"FontColor\":null,\"Width\":null,\"Height\":null,\"PropertyName\":\"bottomSpecialValue\",\"PropertyValue\":\"\",\"isVisible\":0,\"XPosition\":50,\"YPosition\":290,\"PropertyType\":\"text\",\"Alignment\":\"left\",\"ComponentSetPropertyId\":22,\"ComponentPropertyId\":22}]}}";
    const jsonBody = JSON.parse(body);
    console.log({ jsonBody });
};

runHandler(endpoint);
