const path = require("path");

module.exports = {
  entry: {
    getComponentById: "./src/getComponentById.js",
  },
  mode: "production",
  output: {
    filename: "[name].js",
    libraryTarget: "commonjs2",
    path: path.resolve(__dirname, "./build"),
  },
  resolve: {
    extensions: [".js"],
  },
  target: "node",
  node: {
    __dirname: false,
  },
  module: {
    rules: [
      {
        test: /\.node$/,
        loader: "node-loader",
      },
    ],
  },
};