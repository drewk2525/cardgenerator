FROM amazonlinux:latest
RUN yum install gcc openssl-devel bzip2-devel libffi-devel -y
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app
COPY package.json ./
COPY README.md ./
COPY src/ ./src/
COPY resources/ ./
RUN curl -sL https://rpm.nodesource.com/setup_12.x | bash -
RUN yum install -y nodejs
# RUN curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.34.0/install.sh | bash
# RUN . ~/.nvm/nvm.sh
# RUN source ~/.nvm/nvm.sh
# RUN export NVM_DIR="$HOME/.nvm"
# RUN nvm insall node
RUN npm install --production

# FROM node:8-alpine
# RUN yum install gcc openssl-devel bzip2-devel libffi-devel
# RUN mkdir -p /usr/src/app
# WORKDIR /usr/src/app
# COPY package.json ./
# COPY README.md ./
# COPY src/* ./
# COPY resources/* ./
# RUN npm install
# EXPOSE 3000
# CMD [ "node", "server.js" ]