const { writeS3 } = require('./aws/s3');
const { fileNameNormalizer } = require('./utils/fileNameNormalizer');
// const formidable = require('formidable');
const multipart = require('aws-lambda-multipart-parser');
const fs = require('fs');

exports.handler = async (event) => {
    const buff = Buffer.from(event.body, 'base64');
    const decodedEventBody = buff.toString('latin1');
    const spotText = false;
    const decodedEvent = { ...event, body: decodedEventBody };
    const jsonEvent = multipart.parse(decodedEvent, spotText);
    const asset = Buffer.from(jsonEvent.file.content, 'latin1');
    const fileName = fileNameNormalizer(jsonEvent.file.filename);
    const game = fileNameNormalizer(jsonEvent.gameName);
    const fileDir = 'dev';  // TODO - update to use userId
    const location = `assets/${fileDir}/${game}/${fileName}`;

    const assetUrl = await writeS3(asset, location);
    if(event.requestContext.stage === 'local'){
        console.log(`writing locally to: ./${fileName}`);
        fs.writeFileSync(`./${fileName}`, asset);
    }

    const response = {
        isBase64Encoded: false,
        statusCode: 200,
        headers: {},
        multiValueHeaders: {},
        body: JSON.stringify({ assetUrl })
    };
    console.log({ innerResponse: response });
    return response;
};
