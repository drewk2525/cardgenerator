// const mysqlssh = require("mysql-ssh");
const mysqlssh = require('./mysqlSsh');
const AWS = require('aws-sdk');
const ssm = new AWS.SSM({region: 'us-east-1'});

const queryDB = async (query) => {
    const {sshConfig, dbConfig} = await getCredentials();
    let response;
    await mysqlssh
        .connect(sshConfig, dbConfig)
        .then(async (client) => {
            response = await client.query(query).catch((err) => { console.error('Error executing query', err);});
        })
        .catch((err) => {
            console.error('SSH Error', err);
        });
    return response[0];
};

const transactQueryDB = async (queryArray) => {
    const {sshConfig, dbConfig} = await getCredentials();
    // Rework this to be resistant to SQL Inject
    // OR rework to use mysql2 transaction: https://github.com/mysqljs/mysql#transactions
    const combinedQuery = queryArray.reduce((acc, query) => {
        return `${acc} ${query}\n`;
    }, '');
    const query = `START TRANSACTION;\n ${combinedQuery}\n COMMIT;`;

    let response;
    await mysqlssh
        .connect(sshConfig, dbConfig)
        .then(async (client) => {
            response = await client.query(query).catch((err) => { console.error('Error executing query', err);});
        })
        .catch((err) => {
            console.error('SSH Error', err);
        });
    return response;
};

const multiQueryDB = async (queryArray) => {
    const {sshConfig, dbConfig} = await getCredentials();
    let responses;
    await mysqlssh
        .connect(sshConfig, dbConfig)
        .then(async (client) => {
            responses = queryArray.map((query) => {
                return client.query(query).catch((err) => { console.error('Error executing query', err);});
            });
        })
        .catch((err) => {
            console.error('SSH Error', err);
        });
    return responses;
};

const getCredentials = async () => {
    const params = {
        Names: ['sshLogin', 'mySqlLogin'],
        WithDecryption: true
    };
    const { Parameters: parameters } = await ssm.getParameters(params).promise();
    const sshResult = parameters.find((parameter) => { return parameter.Name === 'sshLogin'; });
    const dbResult = parameters.find((parameter) => { return parameter.Name === 'mySqlLogin'; });

    const sshConfig = {...JSON.parse(sshResult.Value)};
    sshConfig.host = 'andrewkroft.com';
    
    const dbConfig = {...JSON.parse(dbResult.Value)};
    dbConfig.host = 'andrewkroft.com';
    dbConfig.database = 'assetGenerator';
    dbConfig.multipleStatements = true;

    return {sshConfig, dbConfig};
};

module.exports = { queryDB, multiQueryDB, transactQueryDB };
