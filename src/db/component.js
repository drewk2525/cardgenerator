const { queryDB, transactQueryDB } = require('./mysql');
const getComponent = async (componentId) => {
    const query = `SELECT
      csp.zPos ZPosition,
      csp.id SecondarySort,
      cs.name ComponentSetName,
      cs.componentWidth ComponentWidth,
      cs.componentHeight ComponentHeight,
      csp.fontSize FontSize,
      csp.fontColor FontColor,
      csp.width Width,
      csp.height Height,
      c.name ComponentName,
      csp.name PropertyName,
      IFNULL(NULLIF(cp.value, ''), csp.defaultValue) PropertyValue,
      cp.isVisible isVisible,
      IFNULL(cp.xPos, csp.xPos) XPosition,
      IFNULL(cp.yPos, csp.yPos) YPosition,
      pt.name PropertyType,
      av.value Alignment,
      csp.id ComponentSetPropertyId,
      cp.id ComponentPropertyId,
      cs.id ComponentSetId,
      c.id ComponentId
  FROM Components c
  JOIN ComponentSets cs
      ON cs.id = c.componentSet_id
  JOIN ComponentSetProperties csp
      ON csp.componentSet_id = cs.id
  JOIN ComponentProperties cp
      ON cp.component_id = c.id
      AND cp.componentSetProperty_id = csp.id
  JOIN LK_PropertyTypes pt
      ON pt.id = csp.propertyType_id
  JOIN LK_AlignmentValues av
     ON av.id = csp.alignment_id
  WHERE c.id = ${componentId}
  ORDER BY ZPosition, SecondarySort;`;
    const results = await queryDB(query);
    return formatComponentJSON(results);
};

const createComponent = async (component) => {
    const componentQuery = `INSERT INTO Components
            (componentSet_id, name)
            VALUES
            (${component.ComponentSetId}, "${component.ComponentName}");`;
    const componentResult = await queryDB(componentQuery);
    console.log({ componentResult });
    const componentId = componentResult.insertId;
    const queries = component.Properties.map((row) => {
        const query = `INSERT INTO ComponentProperties
        (component_id, 
            componentSetProperty_id, 
            value, 
            isVisible, 
            xPos, 
            yPos, 
            width, 
            height)
        VALUES
        (${componentId}, 
            ${row.ComponentSetPropertyId},
            "${row.PropertyValue}", 
            ${row.isVisible}, 
            ${row.XPosition}, 
            ${row.YPosition}, 
            ${row.Width}, 
            ${row.Height});`;
        return query;
    });
    const results = await transactQueryDB(queries);
    return results;
};

const saveComponent = async (component) => {
    const queries = component.Properties.map((row) => {
        const query = `UPDATE ComponentProperties
              SET value = "${row.PropertyValue}",
              isVisible = ${row.isVisible},
              xPos = ${row.XPosition},
              yPos = ${row.YPosition},
              width = ${row.Width},
              height = ${row.Height}
          WHERE id = ${row.ComponentPropertyId};`;
        return query;
    });
    const componentQuery = `UPDATE Components
            SET name = "${component.ComponentName}"
        WHERE id = ${component.ComponentId};`;
    queries.push(componentQuery);
    const results = await transactQueryDB(queries);
    return results;
};

const getComponentSets = async () => {
    const query = 'SELECT * FROM ComponentSets;';
    const result = await queryDB(query);
    return result;
};

const getComponentSetProperties = async (setId) => {
    const query = `SELECT * FROM ComponentSetProperties WHERE componentSet_id = ${setId};`;
    const result = await queryDB(query);
    return result;
};

const formatComponentJSON = (component) => {

    const Properties = component.map((element) => {
        const {
            ComponentSetName,
            ComponentName,
            ComponentWidth,
            ComponentHeight,
            ComponentSetId,
            ComponentId,
            ...props
        } = element;
        return props;
    });
    console.log({ component });
    return {
        ComponentSetName: component[0].ComponentSetName,
        ComponentName: component[0].ComponentName,
        ComponentWidth: component[0].ComponentWidth,
        ComponentHeight: component[0].ComponentHeight,
        ComponentSetId: component[0].ComponentSetId,
        ComponentId: component[0].ComponentId,
        Properties,
    };
};

module.exports = { getComponent, saveComponent, createComponent, getComponentSets, getComponentSetProperties };
