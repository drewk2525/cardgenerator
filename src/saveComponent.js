const { generateAsset } = require('./generateAsset/generateAsset');
const { saveComponent, createComponent } = require('./db/component');
const { writeS3 } = require('./aws/s3');
const fs = require('fs');

exports.handler = async (event) => {
    let statusCode = 200;
    const fileName = 'testFile.jpg';
    try {
        console.log({ event });
        const requestBody = JSON.parse(event.body);
        const component = requestBody.component;
        let newComponent;
        if(component.ComponentId){
            newComponent = await saveComponent(component);
        } else {
            newComponent = await createComponent(component);
        }
        if(!newComponent){
            throw new Error('SQL Error: Failed to save component');
        }
        console.log('Saved Component');

        const asset = await generateAsset(component);
        console.log({asset});
        if(event.requestContext.stage === 'local'){
            fs.writeFileSync('./test.png', asset);
        }
        await writeS3(asset, fileName);
    } catch (error) {
        console.error('there was an error saving the component', error);
        statusCode = 500;
    }

    const bucket = 'dkroft-asset-generator-asset-bucket';
    const imageUrl = statusCode === 200 ? `https://${bucket}.s3.amazonaws.com/${fileName}` : undefined;
    const response = {
        isBase64Encoded: false,
        statusCode,
        headers: {},
        multiValueHeaders: {},
        body: JSON.stringify({ imageUrl }),
    };
    console.log({ innerResponse: response });
    return response;
};
