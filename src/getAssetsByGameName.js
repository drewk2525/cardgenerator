const { listObjects } = require('./aws/s3');
const { fileNameNormalizer } = require('./utils/fileNameNormalizer');

exports.handler = async (event) => {
    const requestBody = JSON.parse(event.body);
    const gameName = fileNameNormalizer(requestBody.gameName);
    const fileDir = 'dev';  // TODO - update to use userId
    const s3Bucket = `assets/${fileDir}/${gameName}`;

    const assets = await listObjects(s3Bucket);

    const response = {
        isBase64Encoded: false,
        statusCode: 200,
        headers: {},
        multiValueHeaders: {},
        body: JSON.stringify({ assets })
    };
    console.log({ innerResponse: response });
    return response;
};
