const { generateAsset } = require('./generateAsset/generateAsset');
const { getComponent } = require('./db/component');
const { writeS3 } = require('./aws/s3');
const { fileNameNormalizer } = require('./utils/fileNameNormalizer');
const fs = require('fs');

exports.handler = async (event) => {
    const requestBody = JSON.parse(event.body);
    const componentId = requestBody.componentId;
    const component = await getComponent(componentId);
    
    const asset = await generateAsset(component);
    if(event.requestContext.stage === 'local'){
        fs.writeFileSync('./test.png', asset);
    }
    const fileDir = 'dev';  // TODO - update to use userId
    const version = 'current';
    const componentName = fileNameNormalizer(component.ComponentName);

    const fileName = `${fileDir}/${version}/${componentName}.jpg`;
    const imageUrl = await writeS3(asset, fileName);

    const response = {
        isBase64Encoded: false,
        statusCode: 200,
        headers: {},
        multiValueHeaders: {},
        body: JSON.stringify({ component,imageUrl })
    };
    console.log({ innerResponse: response });
    return response;
};
