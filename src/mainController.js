const { handler: getComponentById } = require('./getComponentById');
const { handler: saveComponent } = require('./saveComponent');
const { handler: getComponentSets } = require('./getComponentSets');
const { handler: getComponentSetPropertiesById } = require('./getComponentSetPropertiesById');
const { handler: saveAsset } = require('./saveAsset');
const { handler: getAssetsByGameName } = require('./getAssetsByGameName');

exports.handler = async (event, context) => {
    console.log({ event, context });
    const [,,endpoint] = event.requestContext.path.split('/');

    const defaultResponse = {
        isBase64Encoded: false,
        statusCode: 500,
        headers: {},
        multiValueHeaders: {},
        body: JSON.stringify({ message: 'Failure' }),
    };

    console.log({ endpoint });
    let response;
    switch (endpoint) {
    case 'getComponentById':
        response = await getComponentById(event, context);
        break;
    case 'saveComponent':
        response = await saveComponent(event, context);
        break;
    case 'getComponentSets':
        response = await getComponentSets(event, context);
        break;
    case 'getComponentSetPropertiesById':
        response = await getComponentSetPropertiesById(event, context);
        break;
    case 'saveAsset':
        response = await saveAsset(event, context);
        break;
    case 'getAssetsByGameName':
        response = await getAssetsByGameName(event, context);
    }

    response.headers['Content-Type'] = 'application/json';
    response.headers['Access-Control-Allow-Origin'] = '*';
    console.log({ event, context });
    console.log({ response });
    return response || defaultResponse;
};
