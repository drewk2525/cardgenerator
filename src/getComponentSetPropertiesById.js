const { getComponentSetProperties } = require('./db/component');

exports.handler = async (event) => {
    const requestBody = JSON.parse(event.body);
    const componentSetId = requestBody.componentSetId;
    const componenetSetProperties = await getComponentSetProperties(componentSetId);
    
    const response = {
        isBase64Encoded: false,
        statusCode: 200,
        headers: {},
        multiValueHeaders: {},
        body: JSON.stringify({ componenetSetProperties })
    };
    console.log({ event, innerResponse: response });
    return response;
};
