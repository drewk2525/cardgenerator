const { getComponentSets } = require('./db/component');

exports.handler = async (event) => {
    const componenetSets = await getComponentSets();
    
    const response = {
        isBase64Encoded: false,
        statusCode: 200,
        headers: {},
        multiValueHeaders: {},
        body: JSON.stringify({ componenetSets })
    };
    console.log({ event, innerResponse: response });
    return response;
};
