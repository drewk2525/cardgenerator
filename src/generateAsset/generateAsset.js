const { createCanvas, loadImage, registerFont } = require('canvas');
const drawMultilineText = require('canvas-multiline-text');
const { getFile } = require('../aws/s3');

registerFont('./src/generateAsset/fonts/Roboto-Regular.ttf', { family: 'roboto' });

const generateAsset = async (component) => {
    try {
        const {
            // eslint-disable-next-line no-unused-vars
            ComponentSetName: setName,
            // eslint-disable-next-line no-unused-vars
            ComponentName: componentName,
            ComponentWidth: width,
            ComponentHeight: height,
            Properties: properties,
        } = component;

        const canvas = createCanvas(width, height);
        const context = canvas.getContext('2d');
        context.fillStyle = '#fff';
        context.fillRect(0, 0, width, height);

        const promises = [];
        const imageByteCodes = {};
        // Collect async data for properties
        for (const property of properties) {
            if (!property.isVisible) {
                continue;
            }
            switch (property.PropertyType) {
            case 'image': {
                const s3File = getFile(property.PropertyValue).then((response) => {
                    if (response) {
                        imageByteCodes[property.PropertyValue] = response.Body;
                        // property.byteCode = response.Body;
                    }
                });
                promises.push(s3File);
                break;
            }
            }
        }
        // Wait for async promises to resolve
        await Promise.all(promises);

        // Build component in order - DB Query should order by zPos
        for (const property of properties) {
            if (!property.isVisible) {
                continue;
            }
            switch (property.PropertyType) {
            case 'text': {
                context.fillStyle = property.FontColor || '#000';
                context.textAlign = property.Alignment || 'left';
                const textParams = {
                    rect: {
                        x: parseInt(property.XPosition),
                        y: parseInt(property.YPosition),
                        width: parseInt(property.Width || 100),
                        height: parseInt(property.Height || 30),
                    },
                    font: 'roboto',
                    verbose: false,
                    lineHeight: 1.2,
                    minFontSize: parseInt(property.FontSize || 16),
                    maxFontSize: parseInt(property.FontSize || 16),
                };
                drawMultilineText(context, property.PropertyValue, textParams);
                console.log(`placed text ${property.PropertyValue}`);
                break;
            }
            case 'image': {
                const { XPosition, YPosition, Width, Height } = property;
                const image = await loadImage(imageByteCodes[property.PropertyValue]);
                context.drawImage(
                    image,
                    XPosition,
                    YPosition,
                    Width || image.width,
                    Height || image.height
                );
                console.log(`placed image ${property.PropertyValue}`);
                break;
            }
            }
        }

        console.log({canvas});
        const buffer = canvas.toBuffer('image/png');
        console.log('Finished');
        return buffer;
    } catch (error) {
        console.error('Unexpected error', error);
    }
};

module.exports = { generateAsset };
