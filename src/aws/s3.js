const AWS = require('aws-sdk');
AWS.config.apiVersions = {
    s3: '2006-03-01',
};
const Bucket = 'dkroft-asset-generator-asset-bucket';
const s3 = new AWS.S3();

const writeS3 = async (Body, Key) => {
    const params = {
        Body,
        Key,
        Bucket,
    };
    await s3
        .putObject(params)
        .promise()
        .then((data) => {
            console.log({ data });
        })
        .catch((error) => {
            console.error(`Error Writing ${Key} to ${Bucket}`, error);
            throw new Error(error);
        });
    return `https://${Bucket}.s3.amazonaws.com/${Key}`;
};

const getFile = async (Key) => {
    try {
        const params = { Key, Bucket };
        return s3
            .getObject(params)
            .promise()
            .catch((e) => {
                console.error('Error getting object from S3', { Key, Bucket, error: e });
            });
    } catch (error) {
        console.error(`Cannot find ${Key}`);
        throw new Error(error);
    }
};

const listObjects = async (bucketPath) => {
    try {
        const params = {
            Bucket,
            Prefix: bucketPath
        };
        const result = await s3.listObjectsV2(params).promise().catch((e) => {
            console.error('Error listing objects from S3', { bucketPath, error: e });
        });
        const assets = result.Contents.map((currentItem) => {
            const start = currentItem.Key.lastIndexOf('/') + 1;
            const end = currentItem.Key.length;
            const assetName = currentItem.Key.substring(start, end);
            return {
                assetUrl: `https://${Bucket}.s3.amazonaws.com/${currentItem.Key}`,
                assetName
            };
        });
        return assets;
    } catch (error) {
        console.error(error);
        throw new Error(error);
    }

};

module.exports = { writeS3, getFile, listObjects };
