const fileNameNormalizer = (fileName) => {
    return fileName.replace(/ /g, '_');
};

module.exports = { fileNameNormalizer };