# cardGenerator

- Group (deck, dice, etc.) [Group can have just 1 Asset]
  - Asset (card, dice, etc.) [There can be 1+ Assets per group]
    - Available Properties (images, text, etc.)
      - Property Instances (visible, text value, etc.)

$ aws cloudformation validate-template \
--template-body file://template.yaml

## Local Run
Run the following to test locally: `node runLocal.js ENDPOINT`
Make sure the runLocal.js file is up to date with whatever endpoint you are running.

## Local Deploy  
Run the following to deploy from local: `npm run build && npm run deploy`


## Random Notes
`npm prune --production`
`zip -9 -r ./assetGenerator.zip node_modules/ src/`
`aws s3api put-object --bucket dkroft-asset-generator-deployment-bucket --key assetGeneratorDeployment --body ./assetGenerator.zip`

`sam deploy --stack-name generate-assets --capabilities CAPABILITY_NAMED_IAM --no-fail-on-empty-changeset --template-file ./resources/assetGenerator.yaml --s3-bucket dkroft-asset-generator-deployment-bucket --s3-prefix assetGenerator --parameter-overrides LambdaVersion=$VERSION`

`sam deploy --stack-name generate-assets --capabilities CAPABILITY_NAMED_IAM --no-fail-on-empty-changeset --template-file ./resources/assetGenerator.yaml --s3-bucket dkroft-asset-generator-deployment-bucket --s3-prefix assetGenerator --parameter-overrides LambdaVersion=$VERSION`

`aws cloudformation deploy --stack-name generate-assets --template-file ./resources/assetGenerator.yaml --capabilities CAPABILITY_IAM --no-fail-on-empty-changeset --force-upload --parameter-overrides LambdaVersion=0.0.1`

$ aws cloudformation deploy \
--stack-name lambda-api \
--template-file template.yaml \
--capabilities CAPABILITY_IAM

// Pulled from package.json
// "build": "webpack-cli --config webpack.config.js"
// "build": "rm -rf build/* && npm prune --production && mkdir ./build/node_modules && cp -r ./src/* ./build &&cp -r ./node_modules/* ./build/node_modules && npm i",
// "build": "sam build --build-dir ./.aws-sam/build/ --template ./resources/assetGenerator.yaml",
event.queryStringParameters = {"test": "testing"}

// Stuff to look into
// docker volumes
// configure docker image to mount directory on file system
// docker compose